# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 12:17:16 2021

@author: sshah389
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 17:38:35 2021

@author: sshah389
"""

import serial
import numpy as np
from matplotlib import pyplot 
import time
i=0
N=10
tempearture_out=np.zeros([N])
humidity_out=np.zeros([N])
ser = serial.Serial('COM8', 9600, timeout=1) ##change this to your COMport

while i<N:
    ser.write(b'\x01')
    time.sleep(0.5)
    temp_com=ser.readline()
    time.sleep(0.5)
    out_byte = ser.readline()
    out_string = out_byte.decode()
    out_string1 = out_string.strip()
    out_int = float(out_string1)
    tempearture_out[i]=out_int
    i=i+1
   
    
i=0   
while i<N:  
    ser.write(b'\x02')
    time.sleep(0.5)
    humid_com=ser.readline()
    time.sleep(0.5)
    out_byte =  ser.readline()
    out_string = out_byte.decode()
    out_string1 = out_string.strip()
    out_int = float(out_string1)
    humidity_out[i]=out_int
    i=i+1
pyplot.plot(tempearture_out[1:N])  
pyplot.show() 
pyplot.plot(humidity_out[1:N])  
pyplot.show()  
ser.close()
