# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 17:38:35 2021

@author: sshah389
"""

import serial
import numpy as np
from matplotlib import pyplot 
import sounddevice as sd
import scipy.io.wavfile

i=0
N=100000
output=np.zeros([N])
ser = serial.Serial('COM8', 9600, timeout=1) ##change this to your COMport
while i<N:
    out_byte =  ser.readline()
    out_string = out_byte.decode()
    out_string1 = out_string.strip()
    out_int = int(out_string1)
    output[i]=out_int
    i=i+1
    
pyplot.plot(output)    
ser.close()
fs=16000
sd.play(output, fs)
scipy.io.wavfile.write("Hello.wav", fs, output)
