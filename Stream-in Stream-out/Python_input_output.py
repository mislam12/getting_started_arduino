# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 12:17:16 2021

@author: sshah389
"""


import serial
import numpy as np
from matplotlib import pyplot 

i=0
N=100
output=np.zeros([N])
input_serial=np.zeros([N])
ser = serial.Serial('COM8', 921600, timeout=1) ##change this to your COMport
while i<N:
    input_serial[i]=np.random.randint(1,255)
    ser.write(int(input_serial[i]).to_bytes(1,'big'))
    out_byte =  ser.readline()
    out_string = out_byte.decode()
    out_string1 = out_string.strip()
    out_int = int(out_string1)
    output[i]=out_int
    i=i+1


pyplot.plot(output[5:N],'*')   
pyplot.plot(input_serial[5:N])    
ser.close()
